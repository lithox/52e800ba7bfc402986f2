class Event < ApplicationRecord

  validates :kind, presence: true, acceptance: {accept: ['opening', 'appointment']}
  validates :starts_at, presence: true
  validates :ends_at, presence: true
  validates :weekly_recurring, presence: false, acceptance: {accept: [true, false]}

  # check availabilities of date D
  def self.availabilities(date)

    # nb day diff

    list_of_availabilities = Array.new

    # object construct
    Event.all.each do |event|

      if event.kind === 'opening'
        day_of_the_week = event.starts_at.to_date.cwday

        start_at = date.utcra

        i = 0
        while i < 7
          slots = Array.new
          opening_at = event.starts_at
          hour_start_at = event.starts_at.strftime('%H').to_i * 60 + event.starts_at.strftime('%M').to_i
          hour_end_at = event.ends_at.strftime('%H').to_i * 60 + event.starts_at.strftime('%M').to_i

          if (start_at.to_date.cwday === day_of_the_week and event.weekly_recurring) or (!event.weekly_recurring)
            while hour_start_at < hour_end_at

              # check appointment - already take ?
              if appointment(opening_at.strftime('%H:%M')) === true
                slots.push(opening_at.strftime('%k:%M').squish)
              end

              # up to the next 30 minutes
              hour_start_at = hour_start_at+30
              opening_at = opening_at + 30.minutes
            end
          end

          list_of_availabilities.push(:date => start_at, :slots => slots)

          start_at = start_at + 1.day

          i = i + 1
        end
      end

    end

    return list_of_availabilities
  end

  # Check the appointment hours
  def self.appointment(time)
    Event.all.each do |event|
      if event.kind === 'appointment'
        if time.between?(event.starts_at.strftime("%H:%M"), (event.ends_at - 1.minute).strftime("%H:%M"))
          return false
        end
      end
    end
    return true
  end

end
